# To create resources in GCP using terraform script

A set of terraform configuration files to create resources in GCP. To run these files you must have terraform installed in your system.


---

## The steps to install terraform can be found in the following link:

https://learn.hashicorp.com/tutorials/terraform/install-cli

Once installed, you can check if the terraform has installed properly by getting into your command prompt and type **terraform version**

If it shows the version of the terraform that was installed, it means that the terraform has installed successfully. If not, you can click the above link and troubleshoot.

---

## Working with terraform:

##### Step 1:
Once installed successfully, you can clone this repository into your local system & using command prompt you can go into directory (cd) Meshantra/InfraTemplates and use **terraform init**

This is used to initialize a working directory containing Terraform configuration files. This is the first command that should be run after writing a new Terraform configuration or cloning an existing one from version control. It is safe to run this command multiple times 

This command performs several different initialization steps in order to prepare the current working directory for use with Terraform

---

##### Step 2:

Once initialized, you can check if there are any issues in the existing terraform configuration files by running **terraform validate** If there are any issues this command will throw up those errors. Otherwise, *Success! The configuration is valid* is displayed

---

##### Step 3:

If there are no issues, you can use **terraform plan** which creates an execution plan. By default, creating a plan consists of:

   Reading the current state of any already-existing remote objects to make sure that the Terraform state is up-to-date.
   Comparing the current configuration to the prior state and noting any differences.
   Proposing a set of change actions that should, if applied, make the remote objects match the configuration.

The **plan command alone will not actually carry out the proposed changes**, and so you can use this command to check whether the proposed changes match what you expected before you apply the changes or share your changes with your team for broader review

---

##### Step 4:

Finally, to execute the actions proposed in the **terraform plan**, you can run **terraform apply** 

It will automatically create a new execution plan (as if you had run terraform plan) and then prompt you to approve that plan, before taking the indicated actions. You must type **yes** and then these actions will be executed.

---





