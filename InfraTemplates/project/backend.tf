terraform {
  backend "gcs" {
    bucket = "backend_meshantra_statefile"
    prefix = "dev/project"
    credentials = "./credentials/observability_306006_93d312b644c9.json"
  }
}