variable "project_id" {
  description = "This is the id of the project created in GCP"
  type        = string

}



variable "dns_zone_details" {
  description = "This is the DNS zone & DNS name that is created"
  type = list(object({ dns_zone_name = string
    dns_name               = string
    dns_visibility_network = string
  }))
}




