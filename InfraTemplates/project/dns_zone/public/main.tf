resource "google_dns_managed_zone" "cloud_dns_zone" {
  count    = length(var.dns_zone_details)
  name     = var.dns_zone_details[count.index].dns_zone_name
  dns_name = var.dns_zone_details[count.index].dns_name
  project  = var.project_id
}
