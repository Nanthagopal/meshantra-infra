project_id                = "steerwise-meshantra-dev2"
meshantra_service_account = "meshantra_user"

############ DNS and Armor ################

dns_zone_details = [{
  dns_zone_name = "meshantra-zone3"
  dns_name      = "meshantra.com."
  }, {
  dns_zone_name = "meshantra-zone4"
  dns_name      = "meshantra.net."
}]

