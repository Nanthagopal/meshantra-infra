terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
    google-beta = {
      source = "hashicorp/google-beta"
    }
  }
}

provider "google" {
  credentials = file("${var.service_account}.json")
}

provider "google-beta" {

  credentials = file("${var.service_account}.json")

}
