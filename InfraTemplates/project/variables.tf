
variable "service_account" {
  description = "This is the name of the service account JSON file which will be used by Terraform to access GCP"

}


variable "project_name" {
  description = "This is the name of the project created in GCP"
  type        = string

}

variable "project_id" {
  description = "This is the id of the project created in GCP"
  type        = string

}

variable "org_id" {
  description = "This is the Organization id under which the  project is created in GCP"
  type        = number
}


variable "billing_account" {
  description = "This is the Billing account ID to enable billing for this project"
  type        = string
}

variable "auto_create_network" {
  description = "Set default VPC is need when creating a new project"
  type        = bool
}

################## Service Account ################

variable "service_accounts" {
  description = "This is the Service accounts"
  type        = list(object({
    service_account_id = string
    service_account_name = string
  }))

}


variable "service_account_id" {
  description = "This is the Service account ID"
  type        = string

}

variable "service_account_name" {
  description = "This is the SA Name"
  type        = string

}

variable "key_filename" {
  description = "Service file stored using given file name"
  type        = string

}

variable "role_sa" {
  description = "This is the role assigned to the service account"
  type        = list(any)

}

########### Activate API's ###################

variable "project_api" {
  description = "This is the list of all the API's that will be activated for this project"
  type        = list(string)

}

/////////////////// private DNS Zone //////////

variable "dns_zone_details" {
  description = "This is the DNS zone & DNS name that is created"
  type = list(object({ dns_zone_name = string
    dns_name               = string
    dns_visibility_network = string
  }))
}

variable "public_dns_zone_details" {
  description = "This is the DNS zone & DNS name that is created"
  type = list(object({ dns_zone_name = string
    dns_name               = string
  }))
}

########################### cloud function bucket detail ##########################


variable "bucket_name_function" {
    type = string
    description = "bucket name for function"
}

variable "bucket_location" {
    type = string
    description = "bucket location"
}

variable "bucket_role" {
    type = string
    description = "bucket role"
}

variable "bucket_access" {
    type = string
    description = "bucket access"
}


