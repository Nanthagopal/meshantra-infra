variable "service_account" {
    type = string
    description = "service account credentials"
}

variable "project_id" {
  description = "This is the id of the project created in GCP"
  type        = string
}


variable "project_name" {
  description = "This is the id of the project created in GCP"
  type        = string
}

variable "region_name" {
  type = string
  description = "default region name"

  
}

variable "domain_suffix" {
  type = string
  description = "domain_name"
}

variable "project_code"{
  type=string
  description="project code"
}

variable "customer_code" {
  type=string
  description="customer code"
}

variable "environment" {
  type=string
  description="Environment"
}

variable "dns_zone_name" {
  type=string
  description="DNS Zone Name"
}











#### Cloud Storage Notification


variable "cloud_storage_notification" {
  type = list(object({
    storage_bucket_name = string,
    pubsub_topic_name = string,
    storage_bucket_location=string
    
    
  }))
  description = "all parameter for cloud storage Notification"
}


####################################



########### Cloud PUB/SUB #############

variable "cloud_pub_sub" {
  type = list(object({
    pubsub_topic_name = string
  }))
  description = "all parameter for cloud PUB/SUB"
}


########################################


########### Cloud Key Management #############

variable "cloud_kms" {
  type = list(object({
    kms_keyring_name = string,
    kms_cryptokey_name = string
  }))
  description = "all parameter for cloud Key Management"
}


########################################


########### Cloud Secret Manager #############

variable "cloud_secret" {
  type = list(object({
    secret_id = string
  }))
  description = "all parameter for cloud Secret Manager"
}


########################################


#######################Static Website ##############

variable "static_website" {
  type = list(object({
    name = string,
    storage_bucket_location = string,
    access_level = string,
    access_member = string,
    storage_main_page_suffix = string,
    storage_not_found_page = string,
    domain_prefix=string,
    resource_record_ttl=string

  }))
  description = "all parameter for Static Website"
}

#######################################################











////////////////////////////////////////////////////


//////////////////////// Armor //////////////////////

variable "security_policy_name" {
  description = "THis is the name of the security policy created"
  type        = string
}



variable "security_policy_details" {
  description = "This contains the condition to check and action to be done (either allow or deny requests)"
  type = list(object({
    security_action    = string
    security_priority  = string
    security_source_ip = string
  }))
}


///////////////////////////////////////////////////////

/////////////////////// vpc ///////////////////

variable "network_name" {
  description = "THis is the name of the VPC created"
  type        = string
}


variable "routing_mode" {
  description = "This is the network routing mode used. THis can be regional or Global"
  type        = string

}

variable "auto_create_subnetworks" {
  description = "If set to true it will create subnets when network is created"
  type        = string
  default = "false"
}

variable firewall_rules {
  type = list
  description = "set firewall rules"
  default = []
}

variable "delete_default_internet_gateway_routes" {
  type        = bool
  description = "If set, ensure that all routes within the network specified whose names begin with 'default-route' and with a next hop of 'default-internet-gateway' are deleted"
  default     = false
}

variable "mtu" {
  type        = number
  description = "The network MTU (max transmission unit of packet in network). Must be a value between 1460 and 1500 inclusive. If set to 0 (meaning MTU is unset), the network will default to 1460 automatically."
  default     = 0
}

variable "shared_vpc_host" {
  type        = bool
  description = "Makes this project a Shared VPC host if 'true' (default 'false')"
  default     = false
}

variable "subnets" {
  type        = list(map(string))
  description = "The list of subnets being created"
}

variable "secondary_ranges" {
  type        = map(list(object({ range_name = string, ip_cidr_range = string })))
  description = "Secondary ranges that will be used in some of the subnets"
  default     = {}
}

variable "routes" {
  type        = list(map(string))
  description = "List of routes being created in this VPC"
  default     = []
}


///////////////////////////////////////////////////////////////


/////////////////////nat and cloud routes /////////////////////

variable "router_name" {
  description = "This is the name of the Cloud Router"
  type        = string
}

variable "asn_value" {
  description = "This is the value for asn value"
  type        = string
}

variable "advertise_mode" {
  description = "This is the type of the advertise mode"
  type        = string
}

variable "nat_name" {
  description = "This is the type of the Nat gateway name"
  type        = string
}

variable "min_ports_per_vm" {
  description = "No of vm per port connnect to nat"
  type        = string
}

variable "nat_ip_allocate_option" {
  description = "This is the type of the Nat IP Allocate"
  type        = string
}

variable "source_subnetwork_ip_ranges_to_nat" {
  description = "This is the type of the Nat Method Subnet selection"
  type        = string
}

variable "subnetworks" {
  description = "This is the type of the subnet name"
  type        = list
}

//////////////////////////////////

////////////////////  vpc serverless connector ///////////////////

variable "connector_name" {
    type = string
    description = "This is connector name"
}

variable "connector_ip_range" {
    type = string
    description = "This is connector name"
}

////////////////////////////////////////////////////////////////////


///////////////////////////bucket and function /////////////////////////////




variable "cloud_function_collection" {
  type = list(object({
    function_name = string,
    function_description = string,
    function_runtime = string,
    available_memory_mb = string,
    trigger_http = string,
    entry_point = string,
    function_role = string,
    function_member = string,
    source_archive_file_name = string,
    bucket_name = string,
    function_ingress_setting = string,
    connector_egress_setting = string
  }))
  description = "all parameter for cloud function"
}



# variable "function_name" {
#     type = string
#     description = "function name"
# }

# variable "function_description" {
#     type = string
#     description = "function description"
# }

# variable "function_runtime" {
#     type = string
#     description = "function runtime"
# }

# variable "available_memory_mb" {
#     type = string
#     description = "available memory mb"
# }

# variable "trigger_http" {
#     type = string
#     description = "trigger http"
# }

# variable "entry_point" {
#     type = string
#     description = "entry point"
# }

# variable "function_role" {
#     type = string
#     description = "function role"
# }

# variable "function_member" {
#     type = string
#     description = "function member"
# } 

///////////////////////////////////////////////////



////////////////// cloud run ///////////////////////


variable service_run_name {
  type = string
  description = "set service run name"
}


variable service_account_name {
  type = string
  description = "set run for service name"
}

variable service_image {
  type = string
  description = "set run for image"
}

variable service_env_name {
  type = string
  description = "set run for env name"
}

variable service_env_value {
  type = string
  description = "set run for env value"
}


//////////////////////////////////////////////////////


/////////// private ip /////////////////////////////

variable "private_name" {
    type = string
    description = "this is private name"
}

variable "private_purpose" {
    type = string
    description = "this is private name"
}

variable "private_address_type" {
    type = string
    description = "this is private address type"
}

variable "private_prefix_length" {
    type = string
    description = "this is private address type"
}

#################### vpc peer ################

variable "peer_connection_service" {
    type = string
    description = "this is private address type"
}

################### cloud sql ################

variable "sql_name" {
    type = string
    description = "this is cloud sql instance name"
}

variable "database_version" {
    type = string
    description = "this is cloud sql database version"
}

# variable "region_name" {
#     type = string
#     description = "this is cloud sql region name"
# }

variable "deletion_protection" {
    type = string
    description = "this is cloud sql region name"
}

variable "db_tier" {
    type = string
    description = "this is db tier type"
}

variable "activation_policy" {
    type = string
    description = "this is activation policyname"
}

variable "availability_type" {
    type = string
    description = "this is availability type name"
}

variable "disk_autoresize" {
    type = string
    description = "this is disk autoresize status value"
}

variable "disk_size" {
    type = string
    description = "this is disk size of instance"
}

variable "disk_type" {
    type = string
    description = "this is disk type of instance"
}

variable "pricing_plan" {
    type = string
    description = "this is pricing plan of database"
} 

variable "user_labels" {
    type = string
    description = "this is custom user label"
} 

variable "zone" {
    type = string
    description = "this is name of zone"
} 

variable "backup_configuration" {
    type = map
    description = "this is name of zone"
} 

variable "maintenance_window_day" {
    type = number
    description = "this is windows day for maintenance"
} 

variable "maintenance_window_hour" {
    type = number
    description = "this is windows hour for maintenance"
} 

variable "maintenance_window_update_track" {
    type = string
    description = "this is windows update track for maintenance"
}

variable "db_username" {
    type = string
    sensitive = true
    description = "this is username of DB"
}

variable "db_password" {
    type = string
    sensitive = true
    description = "this is password of DB"
}

variable "database_flags" {
    type = list
    description = "this is password of DB"
}

/////////////////////////////////////////////////

###################### kubernetes ##########################

variable "module_source" {

  description = "This variable is to map the respective module's source"

  type        = string

}

variable "k8s_project_id" {

  description = "The project ID to host the cluster in (required)"

  type        = string

}

variable "name" {

  description = "The name of the cluster (required)"

  type        = string

}

variable "kubernetes_version" {

  description = "The Kubernetes version of the masters. If set to 'latest' it will pull latest available version in the selected region."

  type        = string

}

variable "region" {

  description = "The region to host the cluster in (optional if zonal cluster / required if regional)	"

  type        = string

}

variable "zones" {

  description = "The zones to host the cluster in (optional if regional cluster / required if zonal)"

  type        = list(string)

}

variable "network" {

  description = "The VPC network to host the cluster in (required)"

  type        = string

}

variable "subnetwork" {

  description = "The subnetwork to host the cluster in (required)"

  type        = string

}

variable "ip_range_pods" {

  description = "The name of the secondary subnet ip range to use for pods"

  type        = string

}

variable "ip_range_services" {

  description = "The name of the secondary subnet range to use for services"

  type        = string

}

variable "http_load_balancing" {

  description = "Enable httpload balancer addon"

  type        = bool

}

variable "horizontal_pod_autoscaling" {

  description = "Enable horizontal pod autoscaling addon"

  type        = bool

}

variable "network_policy" {

  description = "Enable network policy addon"

  type        = bool

}


variable "node_pools" {

  description = "List of maps containing node pools"

  type        = list(map(string))

}


variable "node_pools_labels" {

  description = "Map of maps containing node labels by node-pool name"

  type        = map(map(string))


}

variable "node_pools_metadata" {

  description = "Map of maps containing node metadata by node-pool name"

  type        = map(map(string))


}

variable "node_pools_oauth_scopes" {

  description = "Map of lists containing node oauth scopes by node-pool name"

  type        = map(list(string))


}

# variable "node_pools_taints" {

#   description = "Map of lists containing node taints by node-pool name"

#   type        = map(list(object({ key = string, value = string, effect = string })))


# }

variable "node_pools_tags" {

  description = "Map of lists containing node network tags by node-pool name"

  type        = map(list(string))


}

