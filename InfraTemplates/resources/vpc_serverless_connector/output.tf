output "vpc_connector_id" {
  value       = google_vpc_access_connector.connector.id
  description = "name of the vpc serverless connector"
}
