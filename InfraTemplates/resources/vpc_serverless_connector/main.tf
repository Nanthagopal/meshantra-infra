resource "google_vpc_access_connector" "connector" {
  name          = var.connector_name
  ip_cidr_range = var.connector_ip_range
  network       = var.network_name
  project = var.project_id
  region = var.region_name

}