variable "project_id" {
    type = string
    description = "project id"
}

variable "region_name" {
    type = string
    description = "name of the region"
}


variable "connector_name" {
    type = string
    description = "name of the vpc serverless connector"
}

variable "connector_ip_range" {
    type = string
    description = "set ip range for vpc serverless connector"
}

variable "network_name" {
    type = string
    description = "mention the name of network, which the connector belong to that network"
}


