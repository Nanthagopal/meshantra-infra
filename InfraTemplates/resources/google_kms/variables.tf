variable "project_id" {
  description = "Project ID where we need to deploy this resource"
  type        = string

}

variable "kms_keyring_name" {
  description = "Name of the KMS Key Ring"
  type        = string

}


variable "kms_key_location" {
  description = "Location on which the KMS Key should created"
  type        = string

}

variable "kms_cryptokey_name" {
  description = "Name of the Crypto Key Name"
  type        = string

}
variable "project_code"{
  type=string
  description="project code"
}

variable "customer_code" {
  type=string
  description="customer code"
}

variable "environment" {
  type=string
  description="Environment"
}