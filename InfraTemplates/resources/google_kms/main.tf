data "google_kms_key_ring" "key_ring" {
  name     = "${var.project_code}-${var.customer_code}-${var.kms_keyring_name}-${var.environment}"
  location = var.kms_key_location
  project  = var.project_id
}

data "google_kms_crypto_key" "crypto_key" {
  name     = "${var.project_code}-${var.customer_code}-${var.kms_cryptokey_name}-${var.environment}"
  key_ring = "projects/${var.project_id}/locations/${var.kms_key_location}/keyRings/${var.kms_keyring_name}"
}