resource "google_compute_security_policy" "security_policy_basic" {

  name    = var.security_policy_name
  project = var.project_id
  count   = length(var.security_policy_details)

  rule {
    action   = var.security_policy_details[count.index].security_action
    priority = var.security_policy_details[count.index].security_priority
    match {
      versioned_expr = "SRC_IPS_V1"
      config {
        src_ip_ranges = [var.security_policy_details[count.index].security_source_ip]
      }
    }
  }

  rule {
    action   = "deny(404)"
    priority = 200
    match {
      expr {
        expression = "evaluatePreconfiguredExpr('sqli-stable')"
      }
    }
  }

  rule {
    action   = "allow"
    priority = "2147483647"
    match {
      versioned_expr = "SRC_IPS_V1"
      config {
        src_ip_ranges = ["*"]
      }
    }
  }
}

