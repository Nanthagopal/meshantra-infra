variable service_run_name {
  type = string
  description = "set service run name"
}


variable service_account_name {
  type = string
  description = "set run for service name"
}

variable service_image {
  type = string
  description = "set run for image"
}

variable service_env_name {
  type = string
  description = "set run for env name"
}

variable service_env_value {
  type = string
  description = "set run for env value"
}


variable project_id {
  type = string
  description = "set service run location"
}

variable region_name {
  type = string
  description = "set service run location"
}

variable connector_name {
  type = string
  description = "set vpc serverless connector"
}