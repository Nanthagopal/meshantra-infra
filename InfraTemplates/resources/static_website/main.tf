resource "google_dns_record_set" "dns_record_set" {
  managed_zone = var.dns_zone_name
  name         = "${var.domain_prefix}.${var.domain_suffix}."
  type         = "A"
  rrdatas      = [google_compute_global_address.external_ip.address]
  ttl          = var.resource_record_ttl
  project      = var.project_id
}

resource "google_storage_bucket" "bucket_website" {
  name     = "${var.project_code}-${var.customer_code}-${var.name}-${var.environment}"
  location = var.storage_bucket_location
  project  = var.project_id

  website {
    main_page_suffix = var.storage_main_page_suffix
    not_found_page   = var.storage_not_found_page
  }

}

resource "google_storage_default_object_access_control" "bucket_website_access" {
  bucket = "${var.project_code}-${var.customer_code}-${var.name}-${var.environment}"
  role   = var.access_level
  entity = var.access_member
  depends_on = [
    google_storage_bucket.bucket_website
  ]
}


resource "google_compute_global_address" "external_ip" {
  project = var.project_id
  name    = "${var.project_code}-${var.customer_code}-${var.name}-${var.environment}"
}

resource "google_compute_backend_bucket" "bucket_backend" {
  project     = var.project_id
  name        = "${var.project_code}-${var.customer_code}-${var.name}-backend-${var.environment}"
  bucket_name = google_storage_bucket.bucket_website.name
  enable_cdn  = true
}



resource "google_compute_url_map" "website_url_map" {
  project         = var.project_id
  name            ="${var.project_code}-${var.customer_code}-${var.name}-map-${var.environment}"
  default_service = google_compute_backend_bucket.bucket_backend.self_link
}


resource "google_compute_managed_ssl_certificate" "website_certificate" {
  project     = var.project_id
  name        = "${var.project_code}-${var.customer_code}-${var.name}-${var.environment}"
  managed {
    domains = ["${var.domain_prefix}.${var.domain_suffix}"]
  }
}

resource "google_compute_target_https_proxy" "website_proxy" {
  project          = var.project_id
  name             = "${var.project_code}-${var.customer_code}-${var.name}-proxy-${var.environment}"
  url_map          = google_compute_url_map.website_url_map.self_link
  ssl_certificates = [google_compute_managed_ssl_certificate.website_certificate.self_link]
}

resource "google_compute_global_forwarding_rule" "default_forwarding_rule" {
  project               = var.project_id
  name                  = "${var.project_code}-${var.customer_code}-${var.name}-rule-${var.environment}"
  load_balancing_scheme = "EXTERNAL"
  ip_address            = google_compute_global_address.external_ip.address
  ip_protocol           = "TCP"
  port_range            = "443"
  target                = google_compute_target_https_proxy.website_proxy.self_link
}


