variable "project_id" {
  description = "This is the id of the project created in GCP"
  type        = string

}

variable "name" {
  description = "This is name of the storage bucket created"
  type        = string

}

variable "domain_prefix" {
  type = string
  description = "domain_name"
}
variable "domain_suffix" {
  type = string
  description = "domain_name"
}

variable "project_code"{
  type=string
  description="project code"
}

variable "customer_code" {
  type=string
  description="customer code"
}

variable "environment" {
  type=string
  description="Environment"
}


variable "dns_zone_name" {
  type=string
  description="DNS Zone Name"
}

variable "resource_record_ttl" {
  type=string
  description="Resource Record TTL"
}




variable "storage_bucket_location" {
  description = "This is the location on which the storage bucket is created"
  type        = string

}


variable "storage_main_page_suffix" {
  description = "This is the main page suffix (index.html) of the website that is created"
  type        = string

}


variable "storage_not_found_page" {
  description = "This is the not found page of the website that is created"
  type        = string

}

variable "access_level" {
  description = "Level of access"
  type        = string

}
variable "access_member" {
  description = "Access given to whom"
  type        = string

}





