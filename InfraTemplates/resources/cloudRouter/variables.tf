variable "region_name" {
  type = string
  description = "default region name"
}

variable "project_id" {
  type = string
  description = "main project id"
}

variable "network_name" {
  description = "This is the name of the VPC created"
  type        = string
}


variable "router_name" {
  description = "This is the name of the Cloud Router"
  type        = string
}

variable "asn_value" {
  description = "This is the value for asn value"
  type        = string
}

variable "advertise_mode" {
  description = "This is the type of the advertise mode"
  type        = string
}

variable "nat_name" {
  description = "This is the type of the Nat gateway name"
  type        = string
}

variable "min_ports_per_vm" {
  description = "No of vm per port connnect to nat"
  type        = string
}

variable "nat_ip_allocate_option" {
  description = "This is the type of the Nat IP Allocate"
  type        = string
}

variable "source_subnetwork_ip_ranges_to_nat" {
  description = "This is the type of the Nat Method Subnet selection"
  type        = string
}

variable "subnetworks" {
  description = "This is the type of the subnet name"
  type        = list
}






