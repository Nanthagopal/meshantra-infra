resource "google_storage_notification" "notification" {
  bucket         = google_storage_bucket.storage_bucket.name
  payload_format = "JSON_API_V1"
  topic          = google_pubsub_topic.pubsub_topic.id
  event_types    = ["OBJECT_FINALIZE", "OBJECT_METADATA_UPDATE"]
 
  depends_on = [google_pubsub_topic_iam_binding.binding]
}


data "google_storage_project_service_account" "gcs_account" {
project  = var.project_id
}

resource "google_pubsub_topic_iam_binding" "binding" {
  topic   = google_pubsub_topic.pubsub_topic.id
  role    = "roles/pubsub.publisher"
  members = ["serviceAccount:${data.google_storage_project_service_account.gcs_account.email_address}"]
}



resource "google_storage_bucket" "storage_bucket" {
  name     = "${var.project_code}-${var.customer_code}-${var.storage_bucket_name}-${var.environment}"
  location = var.storage_bucket_location
  project  = var.project_id

  

}

resource "google_pubsub_topic" "pubsub_topic" {
  name = "${var.project_code}-${var.customer_code}-${var.pubsub_topic_name}-${var.environment}"
   project  = var.project_id
}