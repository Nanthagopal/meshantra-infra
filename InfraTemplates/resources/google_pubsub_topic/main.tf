resource "google_pubsub_topic" "topic" {
  name = "${var.project_code}-${var.customer_code}-${var.pubsub_topic_name}-${var.environment}"
   project  = var.project_id
}