variable "project_id" {
  description = "Project ID where we need to deploy this resource"
  type        = string

}



variable "pubsub_topic_name" {
  description = "Name of the Pubsub Topic Name"
  type        = string

}
variable "project_code"{
  type=string
  description="project code"
}

variable "customer_code" {
  type=string
  description="customer code"
}

variable "environment" {
  type=string
  description="Environment"
}