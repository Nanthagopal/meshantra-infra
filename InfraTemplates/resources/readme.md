# Endpoint setup

# referrance links

 https://cloud.google.com/endpoints/docs/openapi/get-started-cloud-run

 https://github.com/hashicorp/terraform-provider-google/issues/5528


# Endpoint Setup in GCP following setups ( mentioned setup in gcloud cli command)

1. create new service account

```
gcloud iam service-accounts create service-name SVC_ACC=email-service-account
```

2. create a sample Cloud run and get the service host name from the sample cloud run

```
gcloud run deploy test-run  --image="gcr.io/cloudrun/hello"  --allow-unauthenticated --platform managed

```

 you will get service url:
 https://test-run-vwhqfojvyq-uc.a.run.app

 without https is mentioned as service host
 test-run-vwhqfojvyq-uc.a.run.app

 3. configure host into the open-api.yml

 ```
swagger: '2.0'
info:
  title: Cloud Endpoints + GCF
  description: Sample API on Cloud Endpoints with a Google Cloud Functions backend
  version: 1.0.0
host: { service host name from cloud run }
schemes:
  - https
produces:
  - application/json
paths:
  /hello:
    get:
      summary: Greet a user
      operationId: hello
      x-google-backend:
        address: {cloud function url}
        protocol: h2
      responses:
        '200':
          description: A successful response
          schema:
            type: string
 ```

 4. create a endpoint with open-api.yml

 ```
gcloud endpoints services deploy openapi-functions.yaml
 ```

 5. to enable endpoint service and other required services

 ```
 gcloud services enable endpoint-name
 gcloud services enable servicemanagement.googleapis.com
 gcloud services enable servicecontrol.googleapis.com
 gcloud services enable endpoints.googleapis.com
 ```

 6. build new serverless image with project id and service host and build it. Move the image to google image repo

 Download this script to your local machine where the gcloud SDK is installed.

  https://github.com/GoogleCloudPlatform/esp-v2/blob/master/docker/serverless/gcloud_build_image

Run this command:

```
./gcloud_build_image -s CLOUD_RUN_HOSTNAME \
    -c CONFIG_ID -p PROJECT_ID
```

Finally you get google image repo URL:

gcr.io/ESP_PROJECT_ID/endpoints-runtime-serverless:ESP_VERSION-CLOUD_RUN_HOSTNAME-CONFIG_ID


7. Deploying the ESPv2 container

```
gcloud run deploy CLOUD_RUN_SERVICE_NAME \
  --image="gcr.io/ESP_PROJECT_ID/endpoints-runtime-serverless:ESP_VERSION-CLOUD_RUN_HOSTNAME-CONFIG_ID" \
  --set-env-vars=ESPv2_ARGS=--cors_preset=basic \
  --allow-unauthenticated \
  --platform managed \
  --project PROJECT_ID
```








