resource "google_cloudfunctions_function" "function" {
  name        = var.function_name
  description = var.function_description
  runtime     = var.function_runtime
  project     = var.project_id
  available_memory_mb   = var.available_memory_mb
  source_archive_bucket = var.bucket_name
  source_archive_object = var.source_archive_file_name
  trigger_http          = var.trigger_http
  entry_point           = var.entry_point
  ingress_settings      = var.function_ingress_setting
  region                = var.region_name
  vpc_connector         = var.connector_id
  vpc_connector_egress_settings = var.connector_egress_setting
}

resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = var.project_id
  region         = var.region_name
  cloud_function = google_cloudfunctions_function.function.name
  role   = var.function_role
  member = var.function_member
}

