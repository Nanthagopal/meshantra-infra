variable "network_name" {
    type = string
    description = "this is network name for function"
}

variable "function_name" {
    type = string
    description = "function name"
}

variable "function_description" {
    type = string
    description = "function description"
}

variable "function_runtime" {
    type = string
    description = "function runtime"
}

variable "available_memory_mb" {
    type = string
    description = "available memory mb"
}

variable "trigger_http" {
    type = string
    description = "trigger http"
}

variable "entry_point" {
    type = string
    description = "entry point"
}

variable "function_role" {
    type = string
    description = "function role"
}

variable "function_member" {
    type = string
    description = "function member"
}

variable "project_id" {
    type = string
    description = "project id"
}

variable "region_name" {
    type = string
    description = "region name"
}

variable "connector_id" {
    type = string
    description = "Get id from connector module"
}

variable "source_archive_file_name" {
    type = string
    description = "source file for the cloud function"
}

variable "function_ingress_setting" {
    type = string
    description = "cloud function ingress setting"
}

variable "connector_egress_setting" {
    type = string
    description = "vpc serverless connector egress setting"
}

variable "bucket_name" {
    type = string
    description = "name of the bucket for cloud function source"
}



