variable "project_id" {
  description = "Project ID where we need to deploy this resource"
  type        = string

}

variable "secret_id" {
  description = "Unique secret ID"
  type        = string

}


variable "secret_location" {
  description = "Location on which replica should created"
  type        = string

}

variable "project_code"{
  type=string
  description="project code"
}

variable "customer_code" {
  type=string
  description="customer code"
}

variable "environment" {
  type=string
  description="Environment"
}

