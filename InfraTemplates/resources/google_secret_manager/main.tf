resource "google_secret_manager_secret" "secret-manager" {
  secret_id = "${var.project_code}-${var.customer_code}-${var.secret_id}-${var.environment}"
  project  = var.project_id



  replication {
    user_managed {
      replicas {
        location = var.secret_location
      }
      
    }
  }
}

