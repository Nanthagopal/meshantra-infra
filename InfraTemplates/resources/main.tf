# ###################### Armor ###########################

# module "create_armor" {
#   source = "./armor"
#   project_id = var.project_id
#   security_policy_details = var.security_policy_details
#   security_policy_name = var.security_policy_name
# }


# ###################### static website ####################


# module "static_website" {
#   count = length(var.static_website)
#   source = "./static_website"
#   project_id = var.project_id
#   name = var.static_website[count.index].name
#   storage_bucket_location = var.static_website[count.index].storage_bucket_location
#   access_level = var.static_website[count.index].access_level
#   access_member = var.static_website[count.index].access_member
#   storage_main_page_suffix = var.static_website[count.index].storage_main_page_suffix
#   storage_not_found_page   = var.static_website[count.index].storage_not_found_page
#   resource_record_ttl   = var.static_website[count.index].resource_record_ttl
#   domain_prefix=var.static_website[count.index].domain_prefix
#   domain_suffix=var.domain_suffix
#   project_code=var.project_code
#   customer_code=var.customer_code
#   environment=var.environment
#   dns_zone_name=var.dns_zone_name
# }

################# vpc - subnet - routes - firewall ######################
module "create_vpc" {
   source = "./vpc"
   project_id = var.project_id
   auto_create_subnetworks = var.auto_create_subnetworks
   network_name = var.network_name
   routing_mode = var.routing_mode
   firewall_rules = var.firewall_rules
   delete_default_internet_gateway_routes = var.delete_default_internet_gateway_routes
   mtu = var.mtu
   shared_vpc_host = var.shared_vpc_host
   subnets = var.subnets
   secondary_ranges = var.secondary_ranges
   routes = var.routes
 }






# ################### router and natgateway ######################
 module "create_router_and_nat" {
    #-------------------Cloud Router-----------------------#
  source         = "./cloudRouter"
  project_id     = var.project_id
  router_name    = var.router_name
  asn_value      = var.asn_value
  advertise_mode = var.advertise_mode
  region_name    = var.region_name
  network_name   = var.network_name

  #--------------------Nat Gateway------------------------#
  nat_name                    = var.nat_name
  nat_ip_allocate_option             = var.nat_ip_allocate_option
  source_subnetwork_ip_ranges_to_nat = var.source_subnetwork_ip_ranges_to_nat
  subnetworks         = var.subnetworks
  min_ports_per_vm                = var.min_ports_per_vm

  depends_on = [
    module.create_vpc.network_name
  ]

 }

################# Storage Notification ######################

#  module "google_storage_notification" {
#      count = length(var.cloud_storage_notification)


#     source = "./google_storage_notification"
#     project_id = var.project_id
#     storage_bucket_name = var.cloud_storage_notification[count.index].storage_bucket_name
#     storage_bucket_location = var.cloud_storage_notification[count.index].storage_bucket_location
#     pubsub_topic_name= var.cloud_storage_notification[count.index].pubsub_topic_name
#     project_code=var.project_code
#     customer_code=var.customer_code
#     environment=var.environment
#   }

################# KMS  ######################

#  module "google_kms" {
   
#    count = length(var.cloud_kms)

#     source = "./google_kms"
#     project_id = var.project_id
#     kms_keyring_name = var.cloud_kms[count.index].kms_keyring_name
#     kms_key_location = var.region_name
#     kms_cryptokey_name= var.cloud_kms[count.index].kms_cryptokey_name
#     project_code=var.project_code
#     customer_code=var.customer_code
#     environment=var.environment
#   }

################# PUB SUB  ######################

#  module "google_pubsub" {
#    count = length(var.cloud_pub_sub)
#     source = "./google_pubsub_topic"
#     project_id = var.project_id
#     pubsub_topic_name= var.cloud_pub_sub[count.index].pubsub_topic_name
#     project_code=var.project_code
#     customer_code=var.customer_code
#     environment=var.environment


#   }

################# Secret Manager  ######################

#  module "google_secret_manager" {
#    count = length(var.cloud_secret)
#     source = "./google_secret_manager"
#     project_id = var.project_id
#     secret_id = var.cloud_secret[count.index].secret_id
#     secret_location = var.region_name
#     project_code=var.project_code
#     customer_code=var.customer_code
#     environment=var.environment


#   }


################ VPC serverless connector ############

# module "vpc_serverless_connector" {
#   source = "./vpc_serverless_connector"
#   connector_name = var.connector_name
#   connector_ip_range = var.connector_ip_range
#   network_name = var.network_name
#   region_name = var.region_name
#   project_id = var.project_id


  
# }


# ################ cloud function ######################
# module "cloud_function" {

#   source = "./cloudfunction"
#   count = length(var.cloud_function_collection)

#   //function parameters
#   project_id = var.project_id
#   network_name = var.network_name
#   function_name = var.cloud_function_collection[count.index].function_name
#   function_description = var.cloud_function_collection[count.index].function_description
#   function_runtime = var.cloud_function_collection[count.index].function_runtime
#   available_memory_mb = var.cloud_function_collection[count.index].available_memory_mb
#   trigger_http = var.cloud_function_collection[count.index].trigger_http
#   entry_point = var.cloud_function_collection[count.index].entry_point
#   function_role = var.cloud_function_collection[count.index].function_role
#   function_member = var.cloud_function_collection[count.index].function_member
#   region_name = var.region_name
#   bucket_name =  var.cloud_function_collection[count.index].bucket_name
#   source_archive_file_name =  var.cloud_function_collection[count.index].source_archive_file_name
#   function_ingress_setting = var.cloud_function_collection[count.index].function_ingress_setting
#   connector_id = module.vpc_serverless_connector.vpc_connector_id
#   connector_egress_setting = var.cloud_function_collection[count.index].connector_egress_setting


#   depends_on = [
#     module.vpc_serverless_connector
#   ]
# }

# module "endpoint" {
#   source = "./endpoints"
#   service_run_name = var.service_run_name
#   service_account_name = var.service_account_name
#   service_image = var.service_image
#   service_env_name = var.service_env_name
#   service_env_value = var.service_env_value
#   project_id = var.project_id
#   region_name = var.region_name
#   connector_name = var.connector_name
# }

# module "create_sql" {
#   source = "./cloud_sql"
#   network_name = var.network_name
#   project_id = var.project_id
#   /// private ip 
#   private_name = var.private_name
#   private_purpose = var.private_purpose
#   private_address_type = var.private_address_type
#   private_prefix_length = var.private_prefix_length

#   /////////////// vpc peer ///////////////
#   peer_connection_service = var.peer_connection_service

#   //////////// cloud sql /////////////
#   sql_name = var.sql_name
#   database_version = var.database_version
#   region_name = var.region_name
#   deletion_protection = var.deletion_protection
#   db_tier = var.db_tier
#   activation_policy = var.activation_policy
#   availability_type = var.availability_type
#   disk_autoresize = var.disk_autoresize
#   disk_size = var.disk_size
#   disk_type = var.disk_type
#   pricing_plan = var.pricing_plan
#   user_labels = var.user_labels
#   zone = var.zone
#   backup_configuration = var.backup_configuration
#   maintenance_window_day = var.maintenance_window_day
#   maintenance_window_hour = var.maintenance_window_hour
#   maintenance_window_update_track = var.maintenance_window_update_track
#   db_username = var.db_username
#   db_password = var.db_password
#   database_flags = var.database_flags

  
# }

# module "create_run" {
#   source = "./cloud_run"
# }


###################### Kubernetes #############################




#google_client_config and kubernetes provider must be explicitly specified like the following.

data "google_client_config" "default" {}


provider "kubernetes" {

  #load_config_file       = false

  host                   = "https://${module.gke.endpoint}"

  token                  = data.google_client_config.default.access_token

  cluster_ca_certificate = base64decode(module.gke.ca_certificate)

}


module "gke" {

  source                     = "terraform-google-modules/kubernetes-engine/google"

  project_id                 = var.k8s_project_id

  name                       = var.name

  region                     = var.region

  zones                      = var.zones

  network                    = var.network

  subnetwork                 = var.subnetwork

  ip_range_pods              = var.ip_range_pods

  ip_range_services          = var.ip_range_services

  http_load_balancing        = var.http_load_balancing

  horizontal_pod_autoscaling = var.horizontal_pod_autoscaling

    network_policy             = var.network_policy

  kubernetes_version         = var.kubernetes_version


  node_pools = var.node_pools


  node_pools_oauth_scopes = var.node_pools_oauth_scopes


  node_pools_labels = var.node_pools_labels


  node_pools_metadata = var.node_pools_metadata


#   node_pools_taints = var.node_pools_taints


  node_pools_tags = var.node_pools_tags

}





# module "create_sql" {
#   source = "./cloud_sql"
#   network_name = var.network_name
#   project_id = var.project_id
#   /// private ip 
#   private_name = var.private_name
#   private_purpose = var.private_purpose
#   private_address_type = var.private_address_type
#   private_prefix_length = var.private_prefix_length

#   /////////////// vpc peer ///////////////
#   peer_connection_service = var.peer_connection_service

#   //////////// cloud sql /////////////
#   sql_name = var.sql_name
#   database_version = var.database_version
#   region_name = var.region_name
#   deletion_protection = var.deletion_protection
#   db_tier = var.db_tier
#   activation_policy = var.activation_policy
#   availability_type = var.availability_type
#   disk_autoresize = var.disk_autoresize
#   disk_size = var.disk_size
#   disk_type = var.disk_type
#   pricing_plan = var.pricing_plan
#   user_labels = var.user_labels
#   zone = var.zone
#   backup_configuration = var.backup_configuration
#   maintenance_window_day = var.maintenance_window_day
#   maintenance_window_hour = var.maintenance_window_hour
#   maintenance_window_update_track = var.maintenance_window_update_track
#   db_username = var.db_username
#   db_password = var.db_password
#   database_flags = var.database_flags

  
# }

# module "create_run" {
#   source = "./cloud_run"
# }




