module "vpc_subnet_routes" {
  source = "terraform-google-modules/network/google"
  version = "~> 3.0"

  project_id = var.project_id
  auto_create_subnetworks = var.auto_create_subnetworks
  network_name = var.network_name
  routing_mode = var.routing_mode
  
  
  delete_default_internet_gateway_routes = var.delete_default_internet_gateway_routes
  mtu = var.mtu
  shared_vpc_host = var.shared_vpc_host
  subnets = var.subnets
  secondary_ranges = var.secondary_ranges
  routes = var.routes
  
} 


module "firewall_rules" {
  source       = "terraform-google-modules/network/google//modules/firewall-rules"
  project_id   = var.project_id
  network_name = module.vpc_subnet_routes.network_name

  rules = var.firewall_rules
}