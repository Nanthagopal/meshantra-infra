variable "project_id" {
  description = "This is the id of the project created in GCP"
  type        = string
}

variable "network_name" {
  description = "THis is the name of the VPC created"
  type        = string
}


variable "routing_mode" {
  description = "This is the network routing mode used. THis can be regional or Global"
  type        = string

}

variable "auto_create_subnetworks" {
  description = "If set to true it will create subnets when network is created"
  type        = string
  default = "false"
}

variable firewall_rules {
  type = list
  description = "set firewall rules"
}

variable "delete_default_internet_gateway_routes" {
  type        = bool
  description = "If set, ensure that all routes within the network specified whose names begin with 'default-route' and with a next hop of 'default-internet-gateway' are deleted"
  default     = false
}

variable "mtu" {
  type        = number
  description = "The network MTU (max transmission unit of packet in network). Must be a value between 1460 and 1500 inclusive. If set to 0 (meaning MTU is unset), the network will default to 1460 automatically."
  default     = 0
}

variable "shared_vpc_host" {
  type        = bool
  description = "Makes this project a Shared VPC host if 'true' (default 'false')"
  default     = false
}

variable "subnets" {
  type        = list(map(string))
  description = "The list of subnets being created"
}

variable "secondary_ranges" {
  type        = map(list(object({ range_name = string, ip_cidr_range = string })))
  description = "Secondary ranges that will be used in some of the subnets"
  default     = {}
}

variable "routes" {
  type        = list(map(string))
  description = "List of routes being created in this VPC"
  default     = []
}