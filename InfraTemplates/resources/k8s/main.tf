# google_client_config and kubernetes provider must be explicitly specified like the following.

data "google_client_config" "default" {}



provider "kubernetes" {

  load_config_file       = false

  host                   = "https://${module.gke.endpoint}"

  token                  = data.google_client_config.default.access_token

  cluster_ca_certificate = base64decode(module.gke.ca_certificate)

}



module "gke" {

  source                     = "terraform-google-modules/kubernetes-engine/google"

  project_id                 = var.k8s_project_id

  name                       = var.name

  region                     = var.region

  zones                      = var.zones

  network                    = var.network

  subnetwork                 = var.subnetwork

  ip_range_pods              = var.ip_range_pods

  ip_range_services          = var.ip_range_services

  http_load_balancing        = var.http_load_balancing

  horizontal_pod_autoscaling = var.horizontal_pod_autoscaling

  network_policy             = var.network_policy

  kubernetes_version         = var.kubernetes_version



  node_pools = var.node_pools
  
  node_pools_oauth_scopes = var.node_pools_oauth_scopes



  node_pools_labels = var.node_pools_labels



  node_pools_metadata = var.node_pools_metadata



#   node_pools_taints = var.node_pools_taints



  node_pools_tags = var.node_pools_tags

}