output "cluster_id" {

  value =  module.gke.cluster_id

}



output "endpoint" {

  value =  module.gke.endpoint

}



output "ca_certificate" {

  value =  module.gke.ca_certificate

}